import { Component } from "react";

class Form extends Component{
         constructor(props) {
            super(props);
            this.state = {
                firstName: false,
            };
    }
    // validation of first name
    userFirstName = () => {
        let firstNameVal = document.querySelector(".nameType");
        let fname = document.querySelector(".firstName");
        if (/^[A-Za-z\s]+$/.test(firstNameVal.value) && (firstNameVal.value.length >3 && firstNameVal.value.length < 14) ) {
          firstNameVal.style.borderColor = "green";
          fname.style.color = "";
        } else {
          firstNameVal.style.borderColor = "red";
          fname.style.color = "red";
        }
    }
    // validation of last name
      userLastName = () => {
        let lastNameVal = document.querySelector(".lname");
         let lname = document.querySelector(".lastName");
        if (/^[A-Za-z\s]+$/.test(lastNameVal.value) && (lastNameVal.value.length >3 && lastNameVal.value.length < 14) ) {
          lastNameVal.style.borderColor = "green";
          lname.style.color = "";
        } else {
          lastNameVal.style.borderColor = "red";
            lname.style.color = "red";
        }
    }
    // validation of date
    checkDate = () => {
        let dateVal = document.querySelector(".date");
        let dateText = document.querySelector('.dateText')
        let today = new Date()
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        if (dateVal.value > date) {
            dateVal.style.borderColor = "red";
            dateText.style.color='red'
        } else {
              dateVal.style.borderColor = "green";
              dateText.style.color = "";
        }  
    }
    // Email validation
    userEmail = () => {
        let emailVal = document.querySelector('.email');
        console.log(emailVal.value)
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!emailVal.value.match(filter)) {
            emailVal.style.borderColor = "red";
        }
        else {
            emailVal.style.borderColor = "green";
        }
    }
    // Address Validation
    userAddress = () => {
        let addressVal = document.querySelector('.nameAddress');
        if (addressVal.value.length > 7) {
            addressVal.style.borderColor="green"
        }
        else {
            addressVal.style.borderColor="red"
        }
    }
    render() {
        return (
          <div className="form">
            <div className="userName">
               <div  className="textName"><div className="firstName">First Name</div><input className="nameType" type="text" onKeyUp={this.userFirstName} placeholder="Enter your First name"/></div>  
              <div className="textName"><div  className="lastName">First Name</div><input className="nameType lname" type="text" onKeyUp={this.userLastName} placeholder="Enter your Last name"/></div> 
            </div>
            <div className="userEmail">
               <div  className="textName"><div className='dateText'>Date Of Birth</div><input className="nameType date" type="date" onChange={this.checkDate}  /></div>  
              <div className="textName"><div>Email Address</div><input className="nameType email" type="email" onKeyUp={this.userEmail} placeholder="Your mail id"/></div> 
            </div>
            <div className="userAddress">
              <div className="address"><div>Address</div><input className="nameAddress" type="text" onKeyUp={this.userAddress} /></div> 
            </div>
          </div>
        );
     }
}
export default Form;