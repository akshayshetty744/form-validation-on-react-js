import { Component } from "react";
import From from "./form";
import Form2 from "./form2";
import Form3 from "./form3";
class Container extends Component {
     constructor(props) {
        super(props);
        this.state = {
            counter: 1,
        };
    }

    increment = () => {
        if (this.state.counter < 3) {
             this.setState({
               counter: this.state.counter + 1,
             });
            
        }
    }
    decrement = () => {
        if (this.state.counter > 1) {
             this.setState({
               counter: this.state.counter - 1,
             });
        }
    }
  render() {
    return (
      <div className="bodyContent">
        <div className="childContent">
          <div className="imgDiv">
            <div className="bgImg" style={{backgroundImage:`url(${require(`../images/form${this.state.counter}.jpg`)})`}}></div>
          </div>
          <div className="dataContent">
            <div className="formdata">
                <div className="status">
                    <div className="steps"><p className={(this.state.counter == 1 ? 'one' : 'change')}  style={ this.state.counter> 1 ? { backgroundImage: `url(${require(`../images/1828643.png`)})`} : {backgroundImage: ""} }>{this.state.counter>1?' ':'1'}</p><label>Sign Up</label></div>
                    <div className="steps"><p className={(this.state.counter == 2 ? 'one' : 'change')}   style={ this.state.counter> 2 ? { backgroundImage: `url(${require(`../images/1828643.png`)})`} : {backgroundImage: ""} }>{this.state.counter>2?' ':'2'}</p><label>Message</label></div>
                    <div className="steps"><p className={(this.state.counter == 3 ? 'one' : 'change')}   style={ this.state.counter> 3 ? { backgroundImage: `url(${require(`../images/1828643.png`)})`} : {backgroundImage: ""} }>{this.state.counter>3?' ':'3'}</p><label>Checkbox</label></div>
                </div>
                        <hr></hr>
                        <div className="step">Step{this.state.counter}/3</div>
                        <div className="pageName">{this.state.counter == 1  ? 'Sign UP' : null}{ this.state.counter == 2 ?  'Message' : null}{ this.state.counter == 3? 'CheckBox' : null}</div>
                        {
                            this.state.counter == 1 ?        <From />:null
                        }
                         {
                            this.state.counter == 2 ?        <Form2 />:null
                        }
                        {
                            this.state.counter == 3 ?        <Form3 />:null
                        }
                        <hr></hr>
                        <div className="buttonDiv">
                            <button type="button" className="backbtn" onClick={this.decrement} style={this.state.counter > 1 ? { visibility: 'visible' } : { visibility: "hidden" }} >Back</button>   <button type={this.state.counter == 3 ? "submit" : "button"} className="nextStep" onClick={this.increment}>{ this.state.counter == 3?"submit":"Next Step"}</button>
                       </div>

            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Container;
